const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  name: String,
  interests: [String],
  isSubscribed: Boolean
});

module.exports = mongoose.model('user', userSchema);
