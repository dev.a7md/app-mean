const express = require('express');
const mongoose = require('mongoose');
const logger = require('morgan');
const app = express();

const port = process.env.PORT || 3000;
app.listen(port);

mongoose.connect('mongodb://localhost:27017/temp-db', {useNewUrlParser: true});

app.use(logger('dev'));
app.use(express.json());
app.use('/api', require('./routes'));
