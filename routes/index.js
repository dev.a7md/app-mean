const express = require('express');
const router = express.Router();
const User = require('../models/userModel');

// Get All Users
router.get('/users', async (req, res) => {
  const users = await User.find();
  res.send(users);
});

// Get a Specific User
router.get('/users/:id', async (req, res) => {
  const user = await User.findById(req.params.id);
  res.send(user);
});

// Post a new User
router.post('/users', async (req, res) => {
  const user = new User({
    name: req.body.name,
    interests: req.body.interests,
    isSubscribed: req.body.isSubscribed
  });

  const insertedUser = await user.save();
  res.send(insertedUser);
});

// Update an Existing User
router.put('/users/:id', async (req, res) => {
  try {
    await User.findByIdAndUpdate(req.params.id, req.body);
    const savedUser = await User.findById(req.params.id);
    res.send(savedUser);
  } catch (err) {
    res.status(404).send(err.message);
  }
});

// Delete an Existing User
router.delete('/users/:id', async (req, res) => {
  const user = await User.findById(req.params.id);
  if (user) {
    await user.remove();
    res.send(user);
  } else {
    res.status(404).send('User Doesn\'t Exist');
  }
});

module.exports = router;
